# Docker Compose <a name="DC"></a>

## Step 1: 
We install Docker using the following command: 

    sudo curl -sSL get.docker.com | sh

Once that is executed, we still add the USER (pi) to the docker GROUP. This way we don't have to always write sudo in front of the commands:

    sudo usermod -aG docker pi
    
## Step 2: 
Now we install Docker Compose. 
Docker Compose can be used to configure and start multiple services in combination.
We execute the following commands one after the other:

    sudo apt-get install libffi-dev libssl-dev
    sudo apt-get install -y python3 python3-pip
    sudo apt-get remove python-configparser
    sudo pip3 install docker-compose

We reboot the Pi

    sudo reboot

## Step 3
Now we test the Docker installation:

    docker info
    docker ps 
    docker compose info


# Navigation

[Overview](0_README.md)

[Previous Step](3_Webserver.md)

