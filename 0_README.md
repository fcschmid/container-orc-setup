# Overview <a name="overview"></a>

This tutorial shows how an initial, simple container orchestration can be implemented on a Raspberry Pi using Docker and Docker Compose. We install a LAMP stack first, to test the setup of the System and continue with the inital setup of the container orchestration.

[Hardware Preparation](1_Hardware.md) gives an overview of the required hardware. 

[RaspberryPI Operating System](2_OS.md) links to the current descriptions for the installation of the Raspberry Pi OS operating system.

[Web Server](3_Webserver.md) describes the installation of a web server.

[Docker Compose](4_Docker.md) describes the installation of container orchestration.


# Service Tutorials

Continue with one of the following tutorials:

API-Request: [https://gitlab.com/fcschmid/container-orc-setup](https://gitlab.com/fcschmid/container-orc-setup)
