# Web Server <a name="WS"></a>
This part is optional to test a simple web server and browser access to a Raspberry Pi service.

## Step 1
The first step is the installation of the Apache 2 HTTP server.
The following command downloads and installs Apache2.

    sudo apt install apache2

To check, which version is installed:

    service apache2 status

To check if the installation worked, we enter in the browser either the computer name without domain extension or the IP address
Example:

    http://raspberrypi/ or 192.168.0.xxx.

Thus, from now on files can be uploaded to the directory /var/www. The HTML pages can then be accessed accordingly at http://raspberrypi/filename in the local network.


## Step 2
First, we use a console to get an overview of what already exists.

    ls -la /var/www/html/

Then we rename the default page so that we don't lose it.

    cd /var/www/html  
    mv index.html index-20210426.html   

You can now create your own static HTML pages in the web server directory on the Raspberry Pi.
    sudo nano /var/www/html/index.html

Copy an example-HTML-Site in the file:

    <!DOCTYPE html>
    <html>
    <body>
    
    <h1>My First Heading</h1>
    
    <p>My first paragraph.</p>
    
    </body>
    </html>

Open again your domain (host name or IP-address).

## Step 3
To be able to display dynamic content using PHP, we also install PHP7 with the following command:

    sudo apt install php

Restart the Apache Server

    sudo service apache2 reload  

or

    sudo systemctl reload apache2

For testing we create a PHP file.
For this we change into the directory

    cd /var/www/html

Here we create a new file phpinfo.php

    sudo nano phpinfo.php
    
and write into it

    <?php
        phpinfo();
    ?>

Save with CTRL + O
With CTRL + X we close the editor.

Now we open the address http://raspberrypi/phpinfo.php get to see the information page of PHP.

## Step 4
A final step is the installation of a database. We install MySQL (MariaDB), a very common database system for web servers.
First of all we load the needed packages and confirm the installation:

    sudo apt install mariadb-server php-mysql -y

After that you will be prompted to set a password. Here you should choose a secure password. We write down the password.
After MySQL has been set up, the Pi must be rebooted.

    sudo reboot

## Step 5
MySQL alone is functional, but easier administration is possible via the frontend phpMyAdmin. 
With PHPMyAdmin you can manage the database via a graphical user interface in the browser.
We start again with loading the packages and installing them:

    sudo apt-get install phpmyadmin

Within the installation process, select Apache 2 as the web server.
Click Yes to install the phpMyAdmin administration database
Set a password for phpMyAdmin (it can be the same as the MySQL password) and the installation is complete.

The last step is to connect the freshly installed phpMyAdmin to the Apache web server. With this command you can edit the configuration file apache2.conf with the nano editor:

    sudo nano /etc/apache2/apache2.conf

Move the cursor to the end of the configuration file (you can also simply press the key combination "Ctrl" + "V" repeatedly) and write a new line in the file:

    Include /etc/phpmyadmin/apache.conf

Save the changes using the shortcut Ctrl + O and close the configuration file with Ctrl + x. Then restart the Apache web server using another command in the console:

    sudo /etc/init.d/apache2 restart

or

    sudo service apache2 restart  

From now on you can log in at http://raspberrypi/phpmyadmin/ (or 192.168.0.xxx) with the previously entered password for phpMyAdmin. The default username is root or phpMyAdmin.
Now databases can also be created and managed via the interface.



# Navigation

[Overview](0_README.md)

[Previous Step](2_OS.md)

[Next Step](4_Docker.md)



