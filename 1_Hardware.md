# Hardware Preparation<a name="Hardware"></a>

## Step 1: 
The Raspberry Pi Model 4B is a good base to explore the basic features of scalable microservice architectures: [https://www.raspberrypi.com/products/raspberry-pi-4-model-b/](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/)

You can connect many different sensor boards or even external data sources to the Raspberry Pi to get sample data for the following tasks.
However, basic principles can already be realized with core parameters like the CPU load or temperature of the small computer.

The following extensions are optionally useful to increase the reliability or to realize multiparameter acquisition exemplarily with an extension board:
[https://joy-it.net/de/products/rb-strompi3](https://joy-it.net/de/products/rb-strompi3)
[https://projects.raspberrypi.org/en/projects/getting-started-with-the-sense-hat](https://projects.raspberrypi.org/en/projects/getting-started-with-the-sense-hat)


## Step 2: 
Prepare the Raspberry Pi by mounting extensions if necessary, connecting it to your network (RJ45) and preparing the power supply.



# Navigation

[Overview](0_README.md)

[Next Step](2_OS.md)

