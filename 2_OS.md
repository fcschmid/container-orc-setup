# RaspberryPI Operating System <a name="RPIOS"></a>

## Step 1: 
The operating system can be very easily flashed to the SD card with the Raspberry Pi Imager. 
Download the Raspberry Pi Imager and install the software:
[https://www.raspberrypi.org/software/](https://www.raspberrypi.org/software/)

The following link contains a  video tutorial:
[https://www.raspberrypi.com/documentation/computers/getting-started.html#using-raspberry-pi-imager](https://www.raspberrypi.com/documentation/computers/getting-started.html#using-raspberry-pi-imager)

For this tutorial, a minimal configuration of the Raspberry Pi is sufficient. We select the Raspberry Pi OS Lite Image (64-bit) without desktop support. 
In the imager settings we choose an individual hostname and enable SSH with password identification. We choose an individual username and set a password. Both we note down.

If our network configuration supports DHCP we can start the Raspberry Pi this way.

If your network configurations are restricted to fixed IP Adresses you can try to add the specific address to the end of the cmdline.txt file of the boot image.

    ... ip=<255.255.255.255>
    

## Step 2: 
Insert the SD card and supply the Raspberry Pi with power.

## Step 3: 
Windows:
Raspberry Pi SSH connection with Putty
First we need the small program PuTTY for Windows. You don't even need to install it, just open the putty.exe and enter raspberrypi as host.
[https://tutorials-raspberrypi.de/wp-content/uploads/2014/03/putty.png?ezimgfmt=ng:webp/ngcb1](https://tutorials-raspberrypi.de/wp-content/uploads/2014/03/putty.png?ezimgfmt=ng:webp/ngcb1)

After clicking Open a window opens asking for a login name and password. The default data of the operating system Raspbian is user pi with the password raspberry. If you have set an individual user and password, use those credentials.


Linux/Unix/Mac:
To connect to the Raspberry, we need its IP address.
If the Raspberry is connected to the network and in operation, we can find out its IP address using IP scanner tools, for example.

An alternative is filtering via the RaspberryPi MAC addresses:
All raspberry devices MAC addresses started with B8:27:EB or dc:a6:32 for RPi4 B Models.
nmap is an appropriate tool especially for Linux Systems. The following command executes a scan and reports the IP address for Raspberry Pis in the local network:

    nmap -sP 192.168.1.0/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}'

192.168.1.* represents your local network mask.

If you are on a Mac system you can install nmap via [https://brew.sh/index_de](Homebrew)
On Windows, you might use nmap with WinPcap and Zenmap  [https://www.chip.de/downloads/Nmap_13013407.html](https://www.chip.de/downloads/Nmap_13013407.html) or tools like Wireshark.

Use the ssh command to connect to the Raspberry Pi: 

    ssh user@ipaddress [ENTER]
    Password password [ENTER]

Replace user and password with your selected credentials.

## Step 4: 
Check date and time settings

    timedatectl status

If date and time are not set appropriately:

Option 1: Setting timezone in the basic settings of the Raspberry Pi. For this we go to the Raspi-Config. 

    sudo raspi-config

There we can change the timezone under: "Localisation Options"

Option 2: Install ntp for raspberry

    sudo apt-get install ntp

Verify the installation
 
    sudo service ntp status

Switch to an NTP server pool closest to your location, if appropriate:

    sudo nano /etc/ntp.conf

Details are given by [https://support.ntp.org/Servers/NTPPoolServers](https://support.ntp.org/Servers/NTPPoolServers)

Restart the NTP server

    sudo service ntp restart

Verify that the NTP Server is running

    sudo service ntp status

Option 3: set date and time manually
    sudo date -s 'YYYY-MM-DD HH:MM:SS'



## Step 5 (Optional): 
We can change several different settings of the Raspberry Pi. For this we go to the Raspi-Config. 

    sudo raspi-config

There we change the password under: "Change User Password". 
There we change the language settings under: "Localisation Options"
Example:

     I1 Change Locale: en-US UTF8 
     I4 Change Wi-Fi country: US 

There we change the SSH access under "Interfacing Options".
Example:

     SSH enabled

There we change the usage of the SD card under "Asvanced Options".
Example:

     Expand File System: true

There we change the hostname under "Hostname".

     Example: dockerpi

We reboot the Pi after we made changes to the basic settings: 

    sudo reboot

We can change the network settings with the following configuration file:

    /etc/dhcpcd.conf

Nano is a text editor with which we can modify all kinds of text files. We open the configuration file as follows:

    sudo nano /etc/dhcpcd.conf

In the file we add the following below, for example to set a static IP address.
Example:

    interface eth0
    static ip_address=[192.168.2.30]/24
    static routers=[192.168.2.1]
    static domain_name_servers=[192.168.2.1]

## Step 6: 
Then, we perform an update and upgrade so that the Pi has updated libraries and software versions:

    sudo apt-get update && sudo apt-get upgrade -y

We reboot the system:

    sudo reboot

# Navigation

[Overview](0_README.md)

[Previous Step](1_Hardware.md)

[Next Step](3_Webserver.md)
